﻿using System;
using System.Collections.Generic;
using System.Text;

namespace aspIAteur
{
    class Capteur
    {


        int[] mapScanner = new int[25];


        public Capteur()
        {

        }

        public int[] scanner(int[] map)
        {
            Array.Copy(map, this.mapScanner, map.Length);
            return this.mapScanner;
        }

        public int AvoirScore(Environnement env)
        {
            return env.score;
        }

        ~Capteur()
        {
            Console.WriteLine("Capteur détruit");
        }

    }
}
