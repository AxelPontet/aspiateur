﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace aspIAteur
{
    class Informe
    {
        public Informe()
        {

        }
        public int CalculHeuristique(Noeud n)
        {
            int posrobot = -1;
            int cote = (int)Math.Sqrt(n.map.Length);
            int distmin = 20;
            bool bdist = false;
            // Trouve la position du robot
            for (int i = 0; i < n.map.Length; i++)
            {
                if (n.map[i] >= 4)
                {
                    posrobot = i;
                    break;
                }
            }

            int heuri = n.score;
            for (int i = 0; i < n.map.Length; i++)
            {
                if ((n.map[i] ==1) || (n.map[i] == 2||n.map[i] == 5 || n.map[i] == 6))
                {

                    bdist = true;
                    //heuri -= Math.Abs((posrobot / cote) - (i / cote)) + Math.Abs((posrobot % cote) - (i % cote));
                    if(distmin > Math.Abs((posrobot / cote) - (i / cote)) + Math.Abs((posrobot % cote) - (i % cote)))
                    {
                        distmin = Math.Abs((posrobot / cote) - (i / cote)) + Math.Abs((posrobot % cote) - (i % cote));
                    }
                    heuri += 5;
                }
                else if(n.map[i] == 3||n.map[i]==7){
                    bdist = true;
                    //heuri -= 2*(Math.Abs((posrobot / cote) - (i / cote)) + Math.Abs((posrobot % cote) - (i % cote)));
                    if (distmin > Math.Abs((posrobot / cote) - (i / cote)) + Math.Abs((posrobot % cote) - (i % cote)))
                    {
                        distmin = Math.Abs((posrobot / cote) - (i / cote)) + Math.Abs((posrobot % cote) - (i % cote));
                    }
                    heuri += 10;

                }
            }
            if(bdist)
                heuri -= distmin;
            return heuri;
        }

        // Permet de vérifier si un désirs et atteint.
        public bool goalTest(int[][] désirs, int[] map)
        {

            bool res = false;

            for (int i = 0; i < désirs.Length; i++)
            {
                if (Enumerable.SequenceEqual(désirs[i], map) == true)
                {
                    res = true;
                    break;
                }
            }

            return res;
        }


        public List<Noeud> expand(Noeud node)
        {
            string[] listAct = Agent.CalculActionPossible(node.map);
            List<Noeud> succs = new List<Noeud>();
            int[][] scoreMap;
            foreach (string action in listAct)
            {
                Noeud s = new Noeud();
                s.parent = node;
                s.act = action;
                scoreMap = Agent.update(action, node.map);
                s.score = node.score + scoreMap[0][0];
                Array.Copy(scoreMap[1], s.map, s.map.Length);
                //s.map = scoreMap[1];
                s.profondeur = node.profondeur + 1;
                s.enfant = null;
                succs.Add(s);
            }
            return succs;
        }

        public Noeud ComputeTree(Noeud node, int[][] desirs, List<Noeud> frontiere)
        {
           
            if (goalTest(desirs, node.map) == true)
            {
                return node;
            }
            else if(frontiere.Count == 0)
            {
                
                return null;
            }
            else if(node.profondeur>=7)
            {
                return node;
            }
            else
            {
                List<Noeud> toadd = expand(node);
                frontiere.Remove(node);
                foreach(Noeud toa in toadd)
                {
                    frontiere.Add(toa);
                }
                int maxheuris = CalculHeuristique(frontiere[0]);
                Noeud bestnode = frontiere[0];
                for (int i = 0; i < frontiere.Count; i++)
                {
                    if(CalculHeuristique(frontiere[i]) > maxheuris)
                    {
                        bestnode = frontiere[i];
                        maxheuris = CalculHeuristique(frontiere[i]);
                    }
                }
                return ComputeTree(bestnode, desirs, frontiere);
            }
        }

        public List<string> remonterArbre(Noeud node, List<string> accu)
        {
            if (node.parent != null)
            {
                accu.Add(node.act);
                return remonterArbre(node.parent, accu);
            }
            else
            {
                return accu;
            }

        }



       
    }
}