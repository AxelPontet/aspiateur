using System;
using System.Threading;
namespace aspIAteur
{
    class Environnement
    {
        private int[] map;
        public string type;
        public string proba;
        public int score = 0;
        public int[] Map
        {
            get { return map; }   // get method
            set { map = value; }  // set method
        }


        //Constructeur valué avec une map initiale
        public Environnement(ref int[] mapD, string montype, string maProba)
        {
            this.map = mapD;
            this.type = montype;
            this.proba = maProba;
        }




//Fonction d'ajout de poussière dans l'environnement
        public void addDust()
        {
            Random rnd = new Random();
            int invProba = 0;
            switch (this.proba)
            {
                case "a":
                    invProba = 30;
                    break;
                case "b":
                    invProba = 100;
                    break;
            }




            if (rnd.Next(invProba)==1)
            {
                int pieceSale = rnd.Next(this.map.Length);
                switch(this.map[pieceSale])
                {
                    case 0:
                        this.map[pieceSale] = 1;
                        break;

                    case 2:
                        this.map[pieceSale] = 3;
                        break;

                    case 4:
                        this.map[pieceSale] = 5;
                        break;

                    case 6:
                        this.map[pieceSale] = 7;
                        break;
                }
                this.afficherMap();
            }
        }

//Fonction d'ajout de bijoux 
        public void addJewel()
        {
            Random rnd = new Random();
            int invProba = 0;
            switch (this.proba)
            {
                case "a":
                    invProba = 50;
                    break;
                case "b":
                    invProba = 200;
                    break;
            }
            if (rnd.Next(invProba)==1)
            {
                int pieceBijoux = rnd.Next(this.map.Length);
                switch(this.map[pieceBijoux])
                {
                    case 0:
                        this.map[pieceBijoux] = 2;
                        break;

                    case 1:
                        this.map[pieceBijoux] = 3;
                        break;

                    case 4:
                        this.map[pieceBijoux] = 6;
                        break;

                    case 5:
                        this.map[pieceBijoux] = 7;
                        break;
                }
                this.afficherMap();
            }
        }
 public string convert(int i)
        {
            switch (i){
                case 0:
                    return "   ";
                case 1:
                    return "P  ";
                case 2:
                    return "  B";
                case 3:
                    return "P B";
                case 4:
                    return " A ";
                case 5:
                    return "PA ";
                case 6:
                    return " AB";
                case 7:
                    return "PAB";
            }
            return "ERREUR";
        }
/*Fonction d'affichage de la map (On peut à terme la déplacer dans une autre classe)
*/
        public void afficherMap()
        {
            Console.Clear();
            int compteur = 1;
            string inter = "+---+---+---+---+---+";
            Console.WriteLine(inter);
            foreach(int piece in this.map)
            {
                if (compteur%5==0)
                {
                    Console.WriteLine($"|{convert(piece)}|");
                    Console.WriteLine(inter);
                }
                else
                {

                    Console.Write($"|{convert(piece)}");
                }
                compteur ++;
            }
            Console.WriteLine("");
            if(this.type == "n")
                    Console.WriteLine("Methode non-informée");
                else
                    Console.WriteLine("Methode informée");
                Console.WriteLine("score:"+this.score);
        }


        public void run()
        {
            int compteur = 0;
            while(compteur<10000)
            {
                Thread.Sleep(100);
                compteur++;
                this.addDust();
                this.addJewel();
                


            }
        }

/*Fonction mettant la map a jour lors d'une action du robot*/
        public int update(String action)
        {
            int posrobot = -1;
            int valeurcase = 0;
            // Trouve la position du robot
            for (int i = 0; i < this.map.Length; i++)
            {
                if (this.map[i] >= 4)
                {
                    posrobot = i;
                    valeurcase = this.map[i];
                    break;
                }

            }
            switch (action)
            {
                case "monter":
                    map[posrobot] -= 4;
                    map[posrobot - (int)Math.Sqrt(map.Length)] += 4;
                    score = -1;
                    break;

                case "descendre":
                    map[posrobot] -= 4;
                    map[posrobot + (int)Math.Sqrt(map.Length)] += 4;
                    score = -1;
                    break;

                case "gauche":
                    this.map[posrobot] -= 4;
                    this.map[posrobot - 1] += 4;
                    score = -1;
                    break;

                case "droite":
                    this.map[posrobot] -= 4;
                    this.map[posrobot + 1] += 4;
                    score = -1;
                    break;

                case "aspirer":
                    switch (valeurcase)
                    {
                        case 4:
                            score = -1;
                            break;
                        
                        case 5:
                            this.map[posrobot] = 4;
                            score = 5;
                            break;

                        case 6:
                            this.map[posrobot] = 4;
                            score = -30;
                            break;

                        case 7:
                            this.map[posrobot] = 4;
                            score = -25;
                            break;
                    }
                    break;

                case "ramasser":
                    switch (valeurcase)
                    {
                        case 4:
                            score = -1;
                            break;

                        case 5:
                            score = -1;
                            break;

                        case 6:
                            this.map[posrobot] = 4;
                            score = 5;
                            break;

                        case 7:
                            this.map[posrobot] = 5;
                            score = 5;
                            break;
                    }
                    break;
            }
            return (score);
        }

    }
}