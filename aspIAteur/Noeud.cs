using System;
using System.Collections.Generic;
using System.Text;

namespace aspIAteur
{
    class Noeud
    {
        public int score = new int();
        public int[] map = new int [25];

        public int profondeur = new int();
        public string act = null;

        public Noeud parent = null;
        public List<Noeud> enfant = null;

        public Noeud()
        {
            this.profondeur = 0;
            this.act = new string("");
            this.score = 0;
            this.enfant = new List<Noeud>();
            this.parent = null;
        }
        public Noeud(int [] mapD)
        {
            Array.Copy(mapD, this.map, mapD.Length);
            //this.map = mapD;
            this.profondeur = 0;
            this.act = new string("");
            this.score = 0;
            this.enfant = new List<Noeud>();
            this.parent = null;
        }

    }

}