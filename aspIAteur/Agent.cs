﻿
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace aspIAteur
{
    class Agent
    {

        // Performance

        int score = 0; 

        // Type de résolution du pb : { informée, non informée}
        string type;

        // Effecteur
        Effecteur eff = new Effecteur();

        Boolean apprentissage;

        // Capteur 
        Capteur capteur = new Capteur();


        // Environnement
        Environnement monEnv;

        // Croyance
        int[] mapObservé;

        // Désires 

        int[][] desirs;


        

        // Intentions
        
        List<String> Intentions = new List<String>();


        public Agent(Environnement monenv, string montype, bool app) // ref ? 
        {
            this.monEnv = monenv;
            this.type = montype;
            this.apprentissage = app;
            int[][] des = new int[monenv.Map.Length][];
            for (int i = 0; i < monenv.Map.Length; i++)
            {
                int[] toadd = new int[monenv.Map.Length];
                for (int j = 0; j < monenv.Map.Length; j++)
                {
                    toadd[j] = 0;
                }
                toadd[i] = 4;
                des[i] = toadd;
            }
            this.desirs = des;
        }


        public void scanner(int[] map)
        {
            this.mapObservé = this.capteur.scanner(map);

        }

        public void getScore(Environnement env)
        {
            this.score = this.capteur.AvoirScore(env);
        }

        public void faireAction(String act){
            this.eff.faireAction(act);
            this.mapObservé = Agent.update(act, this.mapObservé)[1];
            this.monEnv.score += this.monEnv.update(act);
            this.monEnv.afficherMap();
        }


        // Méthode à lancer lors du multithreading
        public void run()
        {
            int[] scoreAction = new int[7];
            int indiceAction = 0;
            DateTime tempsDepart = DateTime.Now;
            while(true && indiceAction < 7) {
                // Observation de l'environnement
                this.scanner(this.monEnv.Map);

                // Mise à jour de l'état


                // Méthode non informée

                Noeud root = new Noeud(this.mapObservé);
                if(this.type == "n")
                {
                    Arbre monArbre = new Arbre();
                    monArbre.racine = root;
                    int compteur = 1;
                    Noeud result = null;
                    while(compteur < 20 && result == null)
                    {
                        result = monArbre.profondeurLimit(this.desirs, monArbre.racine, compteur);
                        compteur += 1;
                    }
                    monArbre.remonterArbre(result, this.Intentions);
                }

                else if(this.type == "i")
                {

                // Méthode informée
                    
                    Informe monInforme = new Informe();
                    List<Noeud> Front = new List<Noeud>();
                    Front.Add(root);
                    Noeud res  = monInforme.ComputeTree(root, this.desirs, Front);
                    monInforme.remonterArbre(res, this.Intentions);
                }

                // Affichage des Intentions

                foreach (string intent in this.Intentions)
                {
                    //Console.WriteLine(intent);
                }
                // Choisir les actions a faire dans l'arbre

               


                // Faire les actions 
                
                if(this.apprentissage)
                {
                    int indiceBoucle;
                    if (indiceAction + 1 > this.Intentions.Count)
                        indiceBoucle = this.Intentions.Count;
                    else
                        indiceBoucle = indiceAction + 1;

                    for (int i = 0; i < indiceBoucle; i++)
                    {
                        this.faireAction(this.Intentions[this.Intentions.Count-i-1]);
                        Thread.Sleep(1000);
                    }
                    TimeSpan dur = DateTime.Now - tempsDepart;
                    if (dur>new TimeSpan(0,10,0))
                    {
                        tempsDepart = DateTime.Now;
                        this.getScore(monEnv);
                        scoreAction[indiceAction] = this.score;
                        this.score = 0;
                        indiceAction += 1;
                    }
                }
                else
                {
                    for (int i = 0; i < this.Intentions.Count; i++)
                    {
                        this.faireAction(this.Intentions[this.Intentions.Count-i-1]);
                        Thread.Sleep(1000);
                    }
                }
                
                // Reset des intentions
                this.Intentions = new List<string>();
            }  
            Console.Write("Score pour ");
            Console.Write(1);
            Console.Write(" action : ");
            Console.WriteLine(scoreAction[0]);
            for(int i = 1; i< scoreAction.Length; i++)
            {
                Console.Write("Score pour ");
                Console.Write(i+1);
                Console.Write(" actions : ");
                Console.WriteLine(scoreAction[i]-scoreAction[i-1]);
            }
        }

        /*Fonction mettant la map a jour lors d'une action du robot*/
        public static int[][] update(String action, int[] map)
        {
            int[] mapbis = new int[25];
            Array.Copy(map, mapbis, 25);
            int score = 0;
            int posrobot = -1;
            int valeurcase = 0;
            int[][] resultat = new int[2][];
            // Trouve la position du robot
            for (int i = 0; i < mapbis.Length; i++)
            {
                if (mapbis[i] >= 4)
                {
                    posrobot = i;
                    valeurcase = mapbis[i];
                    break;
                }

            }

            switch (action)
            {
                case "monter":
                    mapbis[posrobot] -= 4;
                    mapbis[posrobot -(int)Math.Sqrt(mapbis.Length)] += 4;
                    score = -1;
                    break;

                case "descendre":
                    mapbis[posrobot] -= 4;
                    mapbis[posrobot + (int)Math.Sqrt(mapbis.Length)] += 4;
                    score = -1;
                    break;

                case "gauche":
                    mapbis[posrobot] -= 4;
                    mapbis[posrobot - 1] += 4;
                    score = -1;
                    break;

                case "droite":
                    mapbis[posrobot] -= 4;
                    mapbis[posrobot + 1] += 4;
                    score = -1;
                    break;

                case "aspirer":
                    switch (valeurcase)
                    {
                        case 4:
                            score = -1;
                            break;

                        case 5:
                            mapbis[posrobot] = 4;
                            score = 5;
                            break;

                        case 6:
                            mapbis[posrobot] = 4;
                            score = -30;
                            break;

                        case 7:
                            mapbis[posrobot] = 4;
                            score = -25;
                            break;
                    }
                    break;

                case "ramasser":
                    switch (valeurcase)
                    {
                        case 4:
                            score = -1;
                            break;

                        case 5:
                            score = -1;
                            break;

                        case 6:
                            mapbis[posrobot] = 4;
                            score = 5;
                            break;

                        case 7:
                            mapbis[posrobot] = 5;
                            score = 5;
                            break;
                    }
                    break;
            }
            int[] scoreFin = {score};
            resultat[0] = scoreFin;
            resultat[1] = mapbis;
            return (resultat);
        }

        /* Fonction renvoyant les actions possibles pour un agent selon sa position */
        public static string[] CalculActionPossible(int[] map)
        {

            List<string> res = new List<string>();
            res.Add("monter");
            res.Add("descendre");
            res.Add("gauche");
            res.Add("droite");
            res.Add("ramasser");
            res.Add("aspirer");

            int posrobot = -1;

            // Trouve la position du robot
            for (int i = 0; i < map.Length; i++)
            {
                if (map[i] >= 4)
                {
                    posrobot = i;
                    break;
                }

            }

            if (posrobot%((int)Math.Sqrt(map.Length)) == 0)
            {
                res.Remove("gauche");

            }
            if (posrobot >= (map.Length - (int)Math.Sqrt(map.Length)))
            {
                res.Remove("descendre");

            }
            if (posrobot % (int)Math.Sqrt(map.Length) == 4)
            {
                res.Remove("droite");

            }
            if (posrobot <= (int)Math.Sqrt(map.Length)-1)
            {
                res.Remove("monter");
            }
            string[] resarray = res.ToArray();
            return (resarray);
        }

        ~Agent()
        {
            Console.WriteLine("Agent détruit");
        }

    }
}
