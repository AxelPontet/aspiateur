﻿using System;

using System.Threading;

namespace aspIAteur
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Saisissez i en minuscule pour lancer la methode informée, n en minuscule pour la méthode non informée puis appuyez sur entrée");
            string saisie = Console.ReadLine();
            Console.WriteLine(saisie);
            Console.Clear();
            while (saisie!="i"&&saisie!="n")
            {
                Console.WriteLine("Il semble que vous n'avez pas tapé de caractère valide, saisissez i en minuscule pour lancer la methode informée, n en minuscule pour la méthode non informée puis appuyez sur entrée");
                saisie = Console.ReadLine();
            }
            Console.WriteLine("Saisissez a en minuscule pour avoir un grand taux d'apparition d'entité ( meilleur pour voir la domination de la méthode informée )");
            Console.WriteLine("Saisissez b en minuscule pour avoir un petit taux d'apparition d'entité ( meilleur pour voir les deux méthodes de façon fluide)");
            string proba = Console.ReadLine();
            Console.WriteLine(proba);
            Console.Clear();
            while (proba != "a" && proba != "b")
            {
                Console.WriteLine("Il semble que vous n'avez pas tapé de caractère valide, saisissez a en minuscule pour un grand taux d'apparition, b en minuscule pour un petit taux d'apparition");
                proba = Console.ReadLine();
            }
            bool apprendre;
            string app = "";
            if (saisie == "i")
            {
                Console.WriteLine("Voulez vous lancer l'apprentissage ? Il se pourrait que cette opération prenne un moment car l'apprentissage va essayer de faire des nombres d'actions différents pendant un certain temps afin de choisir la meilleure solution tapez y en minuscule si oui sinon tapez un caractère au hasard ");
                app = Console.ReadLine();
            }
            apprendre = (app == "y");
            // Taille voulue d'un côté de la carte : Si on veut un carré 5*5, alors on set la variable à 5
            int tailleCote = 5;

            // Map dynamique sur la quelle travaillée
            int[] map = new int[tailleCote*tailleCote];

            map[0] = 4;
            map[4] = 1;
            

            //Test environnement
            Environnement monEnvironnement = new Environnement(ref map, saisie, proba);

            // L'objet agent 
            Agent monAgent = new Agent(monEnvironnement, saisie, apprendre);

            //Affichage de l'environnement avant ajout de poussiere/bijoux
            monEnvironnement.afficherMap();

            Thread TEnvironnement = new Thread(monEnvironnement.run);
            

            // Test du multithreading avec agent
            Thread TAgent = new Thread(monAgent.run);
            TEnvironnement.Start();
            TAgent.Start();
            TEnvironnement.Join();
            TAgent.Join();

 
        }
    }
}
