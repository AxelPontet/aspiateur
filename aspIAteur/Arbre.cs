using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace aspIAteur
{
    class Arbre
    {
        public Noeud racine = new Noeud();

        public List<Noeud> expand(Noeud node)
        {
            string[] listAct = Agent.CalculActionPossible(node.map);
            List<Noeud> succs = new List<Noeud>();
            int [][] scoreMap;
            foreach(string action in listAct)
            {
                Noeud s = new Noeud();
                s.parent = node;
                s.act = action;
                scoreMap = Agent.update(action, node.map);
                s.score = node.score + scoreMap[0][0];
                Array.Copy(scoreMap[1], s.map, s.map.Length);
                //s.map = scoreMap[1];
                s.profondeur = node.profondeur + 1;
                s.enfant = null;
                succs.Add(s);
            }
            return succs;
        }

        public Noeud ComputeTree(Noeud node,int limit)
        {
            node.enfant = expand(node);
            if (node.profondeur >= limit)
            {
                return node;
            }
            else
            {
                foreach(Noeud noeud in node.enfant)
                {
                    ComputeTree(noeud, limit);
                }
                return node;
            }
        }

        // permet de v�rifier si un d�sirs est atteint
        public bool goalTest(int [][] desirs, int [] map)
        {

            bool res = false;

            for(int i = 0; i < desirs.Length; i++)
            {
                if(Enumerable.SequenceEqual(desirs[i], map) == true)
                {
                    res = true;
                    break;
                }
            }

            return res;
        }

        public Noeud profondeurLimit(int[][] desirs, Noeud node, int limit)
        {
            Noeud result = new Noeud();
            if (goalTest(desirs, node.map) == true)
            {
                return node;
            }
            else if(node.profondeur == limit)
            {
                return null;
            }
            else
            {
                foreach(Noeud enf in expand(node))
                {
                    result = profondeurLimit(desirs, enf, limit);
                    if(result != null)
                    {
                        return result;
                    }                
                }
                return null;
            }
        }

        public bool compare(int[] tab1, int[] tab2)
        {
            bool retour = true;
            for(int i = 0; i<tab1.Length;i++)
            {
                if (tab1[i]!=tab2[i])
                    retour = false;
            }
            return retour;
        }

        public List<string> remonterArbre(Noeud node, List<string> accu)
        {
            if (node.parent != null)
            {
                accu.Add(node.act);
                return remonterArbre(node.parent, accu);
            }
            else
            {
                return accu;
            }

        }



        public void affArbre()
        {
            Arbre arb = new Arbre();
            if (this.racine.enfant == null)
            {
                afficherMap(this.racine.map);
            }
            else
            {

                afficherMap(this.racine.map);
                
                foreach(Noeud enf in this.racine.enfant)
                {
                    
                    afficherMap(enf.map);
                    foreach(Noeud enfenf in enf.enfant)
                    {
                        
                        afficherMap(enfenf.map);
                    }
                    
                }
            }
            
        }


        public static void afficherMap(int[] map)
        {
            int compteur = 1;
            string inter = "+-+-+-+-+-+";
            Console.WriteLine(inter);
            foreach(int piece in map)
            {
                if (compteur%5==0)
                {
                    Console.WriteLine($"|{piece}|");
                    Console.WriteLine(inter);
                }
                else
                {

                Console.Write($"|{piece}");
                }
                compteur ++;
            }
            Console.WriteLine("");
        }
    }
}